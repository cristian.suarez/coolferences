# coolferences

Web application used as playground / sandbox during ["Confident Software Delivery"](https://agiletestingdays.com/2018/session/confident-software-delivery/) workshop.

---

- [Dev workflow](#dev-workflow)
- [How-to](#how-to)
  - [Run tests](#run-tests)
  - [Run the application](#run-the-application)

---

## Dev workflow

This project makes use of two long-lived branches for the following purposes:

- `master`: work in progress.
- `production`: this is the default branch and contains the code being already deployed in production.

Additionally, it is possible to create feature branches from `master` to develop independent user stories. Those branches have to be merged back into master when they get ready for release.

## How-to

### Run tests

To run the tests you can use the following command: `npm run test`.

We're using [Jest](https://github.com/facebook/jest) as testing framework and test runner.

### Run the application

`npm run start` makes your app accessible at `localhost:1234` by default.

We're using [parcel](https://parceljs.org/), a zero-configuration bundle which happily takes care of all the plumbing involved in the building process :)

Use the `PORT` environment variable to customize the default port used by `parcel` on runtime:

```sh
PORT=3000 npm run start # makes the application to be available @ localhost:3000
```

---

`coolferences` is licensed under the [GNU General Public License v3.0](https://choosealicense.com/licenses/gpl-3.0/).
