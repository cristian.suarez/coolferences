import { ConferenceJson } from "../../../../src/entities/api/ConferenceJson";
import { ConferenceFactoryBuilder } from "../../../../src/entities/factories/ConferenceFactoryBuilder";

const rawConferenceJson: ConferenceJson = {
  cfp: {
    has_cfp: false,
    link: ""
  },
  coc: {
    has_coc: true,
    link: "https://agiletestingdays.com/code-of-conduct/"
  },
  date: {
    end: "11-16-2018",
    start: "11-11-2018"
  },
  location: {
    city: "Potsdam",
    country: "Germany",
    emoji_country: "🇩🇪"
  },
  name: "Agile Testing Days",
  tags: ["software", "testing"],
  url: "https://agiletestingdays.com/"
};

describe("ConferenceFactory", () => {
  it("properly creates a Conference object", () => {
    const subject = ConferenceFactoryBuilder({ today: new Date() });
    const conference = subject(rawConferenceJson);

    expect(conference.cfpUrl).toEqual(rawConferenceJson.cfp.link);
    expect(conference.cocUrl).toEqual(rawConferenceJson.coc.link);
    expect(conference.date).toEqual(rawConferenceJson.date);
    expect(conference.hasCfp).toEqual(rawConferenceJson.cfp.has_cfp);
    expect(conference.hasCoc).toEqual(rawConferenceJson.coc.has_coc);
    expect(conference.location).toEqual(rawConferenceJson.location);
    expect(conference.name).toEqual(rawConferenceJson.name);
    expect(conference.url).toEqual(rawConferenceJson.url);
  });

  describe("handling conference's conclusion", () => {
    it("creates a concluded conference if it has already ended", () => {
      const subject = ConferenceFactoryBuilder({ today: new Date(2018, 11, 31) }); // Dec 31st 2018
      const conference = subject(rawConferenceJson);

      expect(conference.isConcluded).toBe(true);
    });

    it("does not create a concluded conference if it did not start yet", () => {
      const subject = ConferenceFactoryBuilder({ today: new Date(2018, 9, 13) }); // Oct 13th 2018
      const conference = subject(rawConferenceJson);

      expect(conference.isConcluded).toBe(false);
    });

    it("does not create a concluded conference if it is still in progress", () => {
      const subject = ConferenceFactoryBuilder({ today: new Date(2018, 10, 13) }); // Nov 13th 2018
      const conference = subject(rawConferenceJson);

      expect(conference.isConcluded).toBe(false);
    });
  });
});
