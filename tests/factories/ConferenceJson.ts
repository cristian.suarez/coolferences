import { ConferenceJson } from "../../src/entities/api/ConferenceJson";

export function ConferenceJsonFactory(overrides: Partial<ConferenceJson> = {}): ConferenceJson {
  const defaults = {
    cfp: {
      has_cfp: true,
      link: "fake link coc"
    },
    coc: {
      has_coc: true,
      link: "fake link coc"
    },
    date: {
      end: "01-4-2018",
      start: "01-4-2018"
    },
    location: {
      city: "Oviedo",
      country: "Spain",
      emoji_country: "no emoji"
    },
    name: "fake conf",
    tags: ["tag1", "tag2"],
    url: "fake url"
  };

  return { ...defaults, ...overrides };
}
