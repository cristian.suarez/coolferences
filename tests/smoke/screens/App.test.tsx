import React from "react";

import { shallow } from "enzyme";

import { App } from "../../../src/screens/App";

it("renders without crashing", () => {
  expect(() => shallow(<App />)).not.toThrowError();
});
