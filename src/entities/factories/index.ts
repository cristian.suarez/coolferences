import { ConferenceFactoryBuilder } from "./ConferenceFactoryBuilder";

const ConferenceFactory = ConferenceFactoryBuilder({ today: new Date() });

export { ConferenceFactory };
