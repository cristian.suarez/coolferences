export interface Configuration {
  api: string;
  bugsnag: {
    apiKey: string;
  };
  debug: boolean;
  featureFlags: {
    searchConferences: boolean;
  };
}

export function getConfiguration(): Configuration {
  const API_ENDPOINT = process.env.API_ENDPOINT;
  const BUGSNAG_API_KEY = process.env.BUGSNAG_API_KEY;
  const COOLFERENCES_DEBUG = process.env.COOLFERENCES_DEBUG;
  const FF_SEARCH_CONFERENCES = process.env.FF_SEARCH_CONFERENCES;

  if (!API_ENDPOINT || !BUGSNAG_API_KEY) {
    throw new Error("Bad configuration");
  }

  return {
    api: API_ENDPOINT,
    bugsnag: {
      apiKey: BUGSNAG_API_KEY
    },
    debug: COOLFERENCES_DEBUG === "true",
    featureFlags: {
      searchConferences: FF_SEARCH_CONFERENCES === "true"
    }
  };
}
